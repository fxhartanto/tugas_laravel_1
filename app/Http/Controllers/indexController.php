<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function home(){
        return view('home');
    }

    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        $fname = $request->fname;
        $lname = $request->lname;

        return view('welcome', compact('fname', 'lname'));
    }

    public function table(){
        return view('table');
    }

    
    public function data_table(){
        return view('data_table');
    }

    
}
