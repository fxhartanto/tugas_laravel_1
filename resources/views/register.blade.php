@extends('master')

@section('judul')
    Register
@endsection

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
        @csrf
        <label for="fname">First Name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last Name:</label><br>
        <input type="text" id="lname" name="lname"><br>
        <p>Gender:</p>
        <input type="radio" id="Male" name="gender" value="Male">
        <label for="gender">Male</label><br>
        <input type="radio" id="Female" name="gender" value="Female">
        <label for="gender">Female</label><br>
        <input type="radio" id="Other" name="gender" value="Other">
        <label for="gender">Other</label><br>
        <p><label for="nation">Nationality:</label></p>
        <select name="nation" id="nation">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Japan">Japan</option>
            <option value="Spain">Spain</option>
            <option value="Canada">Canada</option>
        </select><br>
        <p>Language Spoken:</p>
        <input type="checkbox" id="lang1" name="lang1" value="Bahasa Indonesia">
        <label for="lang1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="lang2" name="lang2" value="English">
        <label for="lang1">English</label><br>
        <input type="checkbox" id="lang3" name="lang3" value="Other">
        <label for="lang1">Other</label><br>
        <p><label for="bio">Bio:</label></p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form>
</body>
</html>
@endsection