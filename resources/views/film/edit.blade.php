@extends('master')

@section('judul')
    Edit Cast {{$cast->nama}}
@endsection

@section('content')
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value= "{{$cast->nama}}" id="nama" placeholder="Masukkan Nama Cast">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur" value= "{{$cast->umur}}" id="umur" placeholder="Masukkan Umur">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <textarea type="text" class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection